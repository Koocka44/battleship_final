import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

import com.epam.training.battleship.elements.basic.CoordinatePair;
import com.epam.training.battleship.elements.basic.FieldStatus;
import com.epam.training.battleship.elements.ship.ShipTypeLoader;
import com.epam.training.battleship.shooting.ShotResult;
import com.epam.training.battleship.shooting.algorithms.SuperSecretShootingAlgorithm;
import com.epam.training.battleship.shooting.algorithms.TargetingMode;
import com.epam.training.battleship.table.EnemyTable;
import com.epam.training.battleship.table.EnemyTableHandler;
import com.epam.training.battleship.table.PlayerTable;
import com.epam.training.battleship.table.PlayerTableHandler;

public class Server {

	private static final String SHIPS_FILE = "ships.txt";
	private static ServerSocket myService = null;
	private static Socket clientSocket = null;
	private static BufferedReader input = null;
	private static PrintStream output = null;
	private static EnemyTable enemyTable = null;
	private static PlayerTable myTable = null;
	private static EnemyTableHandler enemyTableHandler = null;
	private static PlayerTableHandler myTableHandler = null;
	private static CoordinatePair target;
	private static boolean gameOver = false;
	private static SuperSecretShootingAlgorithm algorithm = null;
	private static int num_shots = 0;
	private static Set<CoordinatePair> targets = new HashSet<>();

	public static void main(String[] args) {

		// int port = Integer.parseInt(args[0]);
		// int width = Integer.parseInt(args[1]);
		// int height = Integer.parseInt(args[2]);

		int port = 3251;
		int width = 20;
		int height = 20;

		myTable = new PlayerTable(width, height);
		enemyTable = new EnemyTable(width, height);

		myTableHandler = new PlayerTableHandler(myTable);
		enemyTableHandler = new EnemyTableHandler(enemyTable);

		myTableHandler.generateTableFromFile(SHIPS_FILE);
		algorithm = new SuperSecretShootingAlgorithm(enemyTable, ShipTypeLoader.loadShipTypesFromFile(SHIPS_FILE));
		System.out.println(myTable);
		try {
			myService = new ServerSocket(port);
			System.out.println("Waiting for connection.");
			clientSocket = myService.accept();
			clientSocket.setTcpNoDelay(true);
			System.out.println("Connected.");
			input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			output = new PrintStream(clientSocket.getOutputStream());
			String helloMessage = "HELLO " + width + " " + height;
			sendMessage(helloMessage);
			while (!gameOver) {
				target = algorithm.getNextTarget();
				targets.add(target);
				String enemyMessage = waitForEnemy();
				processMessage(enemyMessage);
				if (!gameOver) {
					sendFireMessage(target);
					enemyMessage = waitForEnemy();
					processMessage(enemyMessage);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static final String waitForEnemy() throws IOException {
		System.out.println("Waiting for enemy.");
		String line = input.readLine();
		System.out.println("Received message:" + line);
		return line;
	}

	private static final void processMessage(String message) throws IOException {
		if (message.contains("HIT")) {
			addEnemyField(FieldStatus.DESTROYED);
			System.out.println("it was a hit.");
			algorithm.setResponse(ShotResult.HIT);
			algorithm.setTargetingMode(TargetingMode.DESTROY);
			System.out.println(enemyTable);
		} else if (message.contains("SUNK")) {
			addEnemyField(FieldStatus.DESTROYED);
			algorithm.setResponse(ShotResult.SUNK);
			System.out.println("it was a hit-sunk.");
		} else if (message.contains("LOST")) {
			gameOver = true;
			System.out.println("We won! " + num_shots + "/" + targets.size());
			addEnemyField(FieldStatus.DESTROYED);
			System.out.println(enemyTable);
			System.out.println(myTable);
		} else if (message.contains("MISS")) {
			addEnemyField(FieldStatus.WATER);
			algorithm.setResponse(ShotResult.MISS);
		} else if (message.contains("FIRE")) {
			String[] parts = message.split(" ");
			int x = Integer.parseInt(parts[1]);
			int y = Integer.parseInt(parts[2]);
			ShotResult result = myTable.getShotAt(new CoordinatePair(x, y));
			sendResultMessage(result);
		} else {
			throw new IllegalArgumentException("invalid message: " + message);
		}
	}

	private static void addEnemyField(FieldStatus destroyed) {
		enemyTableHandler.addField(target, destroyed);

	}

	private static final void sendResultMessage(ShotResult result) throws IOException {
		String message;
		if (result.equals(ShotResult.HIT)) {
			message = "HIT";
		} else if (result.equals(ShotResult.MISS)) {
			message = "MISS";

		} else if (result.equals(ShotResult.SUNK)) {
			message = "SUNK";
		} else if (result.equals(ShotResult.LOST)) {
			message = "LOST";
			gameOver = true;
		} else {
			throw new IllegalStateException("invalid shot result from player table");
		}
		sendMessage(message);
	}

	private static final void sendMessage(String message) throws IOException {
		System.out.println("sending message: " + message);
		output.println(message);
	}

	private static final void sendFireMessage(CoordinatePair coordinates) throws IOException {
		String message = String.format("FIRE %d %d", coordinates.getX(), coordinates.getY());
		num_shots++;
		sendMessage(message);
	}
}