import com.epam.training.battleship.shooting.PlayerCannon;
import com.epam.training.battleship.table.PlayerTable;
import com.epam.training.battleship.table.PlayerTableHandler;

public class Main {

	public static void main(String[] args) {
		PlayerTable table = new PlayerTable(20, 20);
		PlayerTableHandler tableHandler = new PlayerTableHandler(table);
		PlayerCannon cannon = new PlayerCannon(table);
		tableHandler.generateTableFromFile("ships.txt");
		tableHandler.printTable();
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 20; j += 2) {
				cannon.shootAt(i, j);
			}
		}
		tableHandler.printTable();
	}

}
