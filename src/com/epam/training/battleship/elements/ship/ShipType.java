package com.epam.training.battleship.elements.ship;

import java.util.HashSet;
import java.util.Set;

import com.epam.training.battleship.elements.basic.CoordinatePair;
import com.epam.training.battleship.elements.basic.Field;
import com.epam.training.battleship.elements.basic.FieldStatus;

public class ShipType {

	private Set<Field> fields;
	private ShipTypeBoundaries boundaries;

	public ShipType(Set<Field> fields) {
		super();
		this.fields = new HashSet<>(fields);
		this.boundaries = new ShipTypeBoundaries(fields);
	}

	public Set<Field> getFields() {
		return this.fields;
	}

	public int getMinX() {
		return this.boundaries.getMinX();
	}

	public int getMaxX() {
		return this.boundaries.getMaxX();
	}

	public int getMinY() {
		return this.boundaries.getMinY();
	}

	public int getMaxY() {
		return this.boundaries.getMaxY();
	}

	public void addField(Field field) {
		fields.add(field);
	}

	public FieldStatus getFieldStatusAt(CoordinatePair coordinates) {
		FieldStatus status = FieldStatus.WATER;
		for (Field field : fields) {
			if (field.isAtCoordinates(coordinates)) {
				status = field.getStatus();
			}
		}
		return status;
	}

	public Set<Field> generateFieldsToReferenceCoordinates(CoordinatePair coordinates) {
		Set<Field> worldCoordinateFields = new HashSet<>();
		for (Field field : fields) {
			worldCoordinateFields.add(field.genereateWorldCoordinatesToReference(coordinates));
		}
		return worldCoordinateFields;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((boundaries == null) ? 0 : boundaries.hashCode());
		result = prime * result + ((fields == null) ? 0 : fields.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShipType other = (ShipType) obj;
		if (boundaries == null) {
			if (other.boundaries != null)
				return false;
		} else if (!boundaries.equals(other.boundaries))
			return false;
		if (fields == null) {
			if (other.fields != null)
				return false;
		} else if (!fields.equals(other.fields))
			return false;
		return true;
	}

}
