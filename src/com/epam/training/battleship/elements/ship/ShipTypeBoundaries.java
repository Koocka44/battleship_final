package com.epam.training.battleship.elements.ship;

import java.util.Set;

import com.epam.training.battleship.elements.basic.Field;

public class ShipTypeBoundaries {

	private final int minX;
	private final int maxX;
	private final int minY;
	private final int maxY;

	public ShipTypeBoundaries(Set<Field> fields) {
		int minX = 3;
		int maxX = 0;
		int minY = 3;
		int maxY = 0;
		for (Field field : fields) {
			int x = field.getX();
			int y = field.getY();
			if (x < minX) {
				minX = x;
			}
			if (x > maxX) {
				maxX = x;
			}
			if (y < minY) {
				minY = y;
			}
			if (y > maxY) {
				maxY = y;
			}
		}
		this.maxX = maxX;
		this.maxY = maxY;
		this.minX = minX;
		this.minY = minY;
	}

	public int getMinX() {
		return minX;
	}

	public int getMaxX() {
		return maxX;
	}

	public int getMinY() {
		return minY;
	}

	public int getMaxY() {
		return maxY;
	}

}
