package com.epam.training.battleship.elements.ship;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.epam.training.battleship.elements.basic.CoordinatePair;
import com.epam.training.battleship.elements.basic.Field;
import com.epam.training.battleship.elements.basic.FieldStatus;
import com.epam.training.battleship.shooting.ShotResult;

public class Ship {

	private Set<Field> fields;
	private boolean isPlaced;
	private final ShipType shipType;

	public Ship(ShipType shipType, CoordinatePair referenceCoordinates) {
		super();
		this.shipType = shipType;
		this.fields = shipType.generateFieldsToReferenceCoordinates(referenceCoordinates);
	}

	public ShipType getShipType() {
		return shipType;
	}

	public boolean hasMoreFields() {
		return fields.size() != 0;
	}

	public CoordinatePair getNextFieldCoordinates() {
		CoordinatePair next = new ArrayList<>(fields).get(0).getCoordinates();
		removeFieldAtCoordinate(next);
		return next;
	}

	public List<Field> getFields() {
		return new ArrayList<>(fields);
	}

	public void removeFieldAtCoordinate(CoordinatePair coordinate) {
		fields.remove(new Field(coordinate, FieldStatus.SHIP));
	}

	public boolean isAtCoordinates(CoordinatePair coordinates) {
		boolean isAtCoordinates = false;
		for (Field field : fields) {
			if (field.isAtCoordinates(coordinates)) {
				isAtCoordinates = true;
			}
		}
		return isAtCoordinates;
	}

	public boolean hasStatusAt(FieldStatus status, CoordinatePair coordinates) {
		for (Field field : fields) {
			if (field.isAtCoordinates(coordinates) && field.hasStatus(status)) {
				return true;
			}
		}
		return false;
	}

	public boolean isPlaced() {
		return this.isPlaced;
	}

	public void putDown() {
		this.isPlaced = true;
	}

	public boolean isInTheWayOfShip(Ship ship) {
		boolean inTheWay = false;
		for (Field currentField : fields) {
			for (Field otherField : ship.fields) {
				if (currentField.atOrNextTo(otherField)) {
					inTheWay = true;
				}
			}
		}
		return inTheWay;
	}

	public boolean areFieldsValidOnSize(int sizeX, int sizeY) {
		for (Field field : fields) {
			if (!field.validOnSize(sizeX, sizeY)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		return "Ship [fields=" + fields + ", isPlaced=" + isPlaced + "]";
	}

	public boolean isDead() {
		boolean dead = true;
		for (Field field : fields) {
			if (field.hasStatus(FieldStatus.SHIP)) {
				dead = false;
			}
		}
		return dead;
	}

	public ShotResult getShotAt(CoordinatePair coordinates) {
		for (Field field : fields) {
			if (field.isAtCoordinates(coordinates)) {
				field.setStatus(FieldStatus.DESTROYED);
				break;
			}
		}
		boolean isDead = isDead();
		return isDead ? ShotResult.SUNK : ShotResult.HIT;
	}

}
