package com.epam.training.battleship.elements.ship;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.epam.training.battleship.elements.basic.CoordinatePair;
import com.epam.training.battleship.elements.basic.Field;
import com.epam.training.battleship.elements.basic.FieldStatus;

public class ShipTypeLoader {

	public static Map<ShipType, Integer> loadShipTypesFromFile(String filename) {
		Map<ShipType, Integer> shipTypesWithCount = new HashMap<ShipType, Integer>();
		Set<Field> fields = new HashSet<Field>();

		try {
			List<String> linesInFile = Files.readAllLines(Paths.get(filename), Charset.defaultCharset());
			int rowCounter = 0;
			for (String line : linesInFile) {
				if (rowCounter == 4) {
					int numberOfThisType = Integer.parseInt(line);
					ShipType shipType = new ShipType(fields);
					shipTypesWithCount.put(shipType, numberOfThisType);
					fields.clear();
					rowCounter = 0;
				} else {
					for (int columnCounter = 0; columnCounter < line.length(); columnCounter++) {
						CoordinatePair currentCoordinate = new CoordinatePair(rowCounter, columnCounter);
						if (Character.toLowerCase(line.charAt(columnCounter)) == 'x') {
							fields.add(new Field(currentCoordinate, FieldStatus.SHIP));
						}
					}
					rowCounter++;
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return shipTypesWithCount;
	}
}
