package com.epam.training.battleship.elements.basic;

public enum FieldStatus {
	WATER, SHIP, DESTROYED, CLOSED
}
