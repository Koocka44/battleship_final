package com.epam.training.battleship.elements.basic;

public class CoordinatePair {

	private final int x;
	private final int y;

	public CoordinatePair(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public CoordinatePair generateWorldCoordinatesFromReference(CoordinatePair referenceCoordinates) {
		return new CoordinatePair(x + referenceCoordinates.x, y + referenceCoordinates.y);
	}

	@Override
	public String toString() {
		return "CoordinatePair [x=" + x + ", y=" + y + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CoordinatePair other = (CoordinatePair) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	public boolean atOrNextTo(CoordinatePair other) {
		int diffX = Math.abs(this.x - other.x);
		int diffY = Math.abs(this.y - other.y);
		return sameCoordinate(diffX, diffY) || neighbor(diffX, diffY) || diagonalNeighbor(diffX, diffY);

	}

	public boolean validOnSize(int sizeX, int sizeY) {
		return !(isNegative() || outOfBounds(sizeX, sizeY));
	}

	private boolean diagonalNeighbor(int diffX, int diffY) {
		return (diffX == 1 && diffY == 1);
	}

	private boolean neighbor(int diffX, int diffY) {
		return (diffX + diffY) == 1;
	}

	private boolean sameCoordinate(int diffX, int diffY) {
		return (diffX == 0 && diffY == 0);
	}

	private boolean outOfBounds(int sizeX, int sizeY) {
		return x >= sizeX || y >= sizeY;
	}

	private boolean isNegative() {
		return (this.x < 0 || this.y < 0);
	}

}
