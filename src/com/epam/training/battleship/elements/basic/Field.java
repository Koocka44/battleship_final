package com.epam.training.battleship.elements.basic;

public class Field {

	private CoordinatePair coordinates;
	private FieldStatus status;

	public Field(CoordinatePair coordinates, FieldStatus status) {
		super();
		this.coordinates = coordinates;
		this.status = status;
	}

	public CoordinatePair getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(CoordinatePair coordinates) {
		this.coordinates = coordinates;
	}

	public FieldStatus getStatus() {
		return status;
	}

	public void setStatus(FieldStatus status) {
		this.status = status;
	}

	public Field genereateWorldCoordinatesToReference(CoordinatePair coordinates) {
		return new Field(this.coordinates.generateWorldCoordinatesFromReference(coordinates), status);
	}

	public boolean isAtCoordinates(CoordinatePair coordinates) {
		return this.coordinates.equals(coordinates);
	}

	@Override
	public String toString() {
		return "Field [coordinates=" + coordinates + ", status=" + status + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coordinates == null) ? 0 : coordinates.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Field other = (Field) obj;
		if (coordinates == null) {
			if (other.coordinates != null)
				return false;
		} else if (!coordinates.equals(other.coordinates))
			return false;
		return true;
	}

	public boolean hasStatus(FieldStatus status) {
		return this.status.equals(status);
	}

	public boolean atOrNextTo(Field otherField) {
		return coordinates.atOrNextTo(otherField.coordinates);
	}

	public boolean validOnSize(int sizeX, int sizeY) {
		return this.coordinates.validOnSize(sizeX, sizeY);
	}

	public int getX() {
		return coordinates.getX();
	}

	public int getY() {
		return coordinates.getY();
	}

}
