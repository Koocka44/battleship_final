package com.epam.training.battleship.table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.epam.training.battleship.elements.basic.CoordinatePair;
import com.epam.training.battleship.elements.basic.Field;
import com.epam.training.battleship.elements.basic.FieldStatus;
import com.epam.training.battleship.elements.ship.Ship;

public class EnemyTable {

	private Set<Field> fields;
	private final int sizeX;
	private final int sizeY;

	public EnemyTable(int sizeX, int sizeY) {
		this.fields = new HashSet<>();
		this.sizeX = sizeX;
		this.sizeY = sizeY;
	}

	public void addField(Field field) {
		fields.add(field);
	}

	public boolean wouldShipFit(Ship ship, CoordinatePair ignore) {
		boolean result = true;
		for (Field field : fields) {
			if (!field.isAtCoordinates(ignore) && ship.isAtCoordinates(field.getCoordinates())) {
				result = false;
			}
		}
		if (!ship.areFieldsValidOnSize(sizeX, sizeY)) {
			result = false;
		}
		return result;
	}

	public int getSizeX() {
		return sizeX;
	}

	public int getSizeY() {
		return sizeY;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < sizeX; i++) {
			for (int j = 0; j < sizeY; j++) {
				CoordinatePair coordinatePair = new CoordinatePair(i, j);
				boolean printed = false;
				for (Field field : fields) {
					if (field.isAtCoordinates(coordinatePair) && field.hasStatus(FieldStatus.SHIP)) {
						sb.append("O ");
						printed = true;
					} else if (field.isAtCoordinates(coordinatePair) && field.hasStatus(FieldStatus.DESTROYED)) {
						sb.append("X ");
						printed = true;
					} else if (field.isAtCoordinates(coordinatePair) && field.hasStatus(FieldStatus.WATER)) {
						sb.append("~ ");
						printed = true;
					} else if (field.isAtCoordinates(coordinatePair) && field.hasStatus(FieldStatus.CLOSED)) {
						sb.append("s ");
						printed = true;
					}
				}
				if (!printed) {
					sb.append(". ");
				}
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	public List<CoordinatePair> generateDontShootCoordinates() {
		System.out.println("gendontshoot");
		List<Field> tempFields = new ArrayList<>();
		List<CoordinatePair> coords = new ArrayList<>();
		for (Field field : fields) {
			if (field.hasStatus(FieldStatus.DESTROYED)) {
				tempFields.addAll(generateFieldsAround(field));
			}
		}
		fields.addAll(tempFields);
		for (Field field : fields) {
			coords.add(field.getCoordinates());
		}
		return coords;

	}

	private Collection<? extends Field> generateFieldsAround(Field field) {
		List<Field> fields = new ArrayList<>();
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				fields.add(new Field(new CoordinatePair(field.getX() + i, field.getY() + j), FieldStatus.CLOSED));
			}
		}
		return fields;
	}
}
