package com.epam.training.battleship.table;

import com.epam.training.battleship.elements.basic.CoordinatePair;
import com.epam.training.battleship.elements.basic.Field;
import com.epam.training.battleship.elements.basic.FieldStatus;

public class EnemyTableHandler {

	private EnemyTable table;

	public EnemyTableHandler(EnemyTable table) {
		this.table = table;
	}

	public void addField(CoordinatePair coordinates, FieldStatus status) {
		table.addField(new Field(coordinates, status));
	}
}
