package com.epam.training.battleship.table;

import java.util.List;
import java.util.Map;

import com.epam.training.battleship.elements.basic.CoordinatePair;
import com.epam.training.battleship.elements.ship.Ship;
import com.epam.training.battleship.elements.ship.ShipType;
import com.epam.training.battleship.elements.ship.ShipTypeLoader;

public class PlayerTableHandler {

	private final PlayerTable table;

	public PlayerTableHandler(PlayerTable table) {
		this.table = table;
	}

	private boolean tryToPutShipDown(ShipType shipType) {
		boolean success = putDownRandomly(shipType);
		if (!success) {
			success = putDownWithBruteForce(shipType);
		}
		return success;
	}

	private boolean putDownWithBruteForce(ShipType shipType) {
		List<CoordinatePair> possibleCoordinates = table.generatePossibleCoordinatesForShipType(shipType);
		for (CoordinatePair coordinates : possibleCoordinates) {
			Ship ship = new Ship(shipType, coordinates);
			table.tryToPutShip(ship);
			if (ship.isPlaced()) {
				return true;
			}
		}
		return false;
	}

	private boolean putDownRandomly(ShipType shipType) {
		int tryCounter = 0;
		while (tryCounter < 10) {
			CoordinatePair baseCoordinates = table.generateRandomCoordinatesForShipType(shipType);
			Ship ship = new Ship(shipType, baseCoordinates);
			table.tryToPutShip(ship);
			tryCounter++;
			if (ship.isPlaced()) {
				return true;
			}
		}
		return false;
	}

	private boolean tryToPutShipsDown(Map<ShipType, Integer> ships) {
		for (ShipType shipType : ships.keySet()) {
			for (int i = 0; i < ships.get(shipType); i++) {
				if (!tryToPutShipDown(shipType)) {
					return false;
				}
			}
		}
		return true;
	}

	public void printTable() {
		System.out.println(table.toString());
	}

	private void putShipsDown(Map<ShipType, Integer> ships) {
		int tryCounter = 0;
		while (tryCounter < 20) {
			if (tryToPutShipsDown(ships)) {
				return;
			}
			tryCounter++;
		}
	}

	public void generateTableFromFile(String filename) {
		putShipsDown(ShipTypeLoader.loadShipTypesFromFile(filename));
	}

}
