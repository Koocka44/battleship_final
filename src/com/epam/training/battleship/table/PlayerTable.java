package com.epam.training.battleship.table;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.epam.training.battleship.elements.basic.CoordinatePair;
import com.epam.training.battleship.elements.basic.FieldStatus;
import com.epam.training.battleship.elements.ship.Ship;
import com.epam.training.battleship.elements.ship.ShipType;
import com.epam.training.battleship.shooting.ShotResult;

public class PlayerTable {

	private List<Ship> ships;
	private final int sizeX;
	private final int sizeY;

	public PlayerTable(int sizeX, int sizeY) {
		super();
		this.ships = new ArrayList<>();
		this.sizeX = sizeX;
		this.sizeY = sizeY;
	}

	public void addShip(Ship ship) {
		ships.add(ship);
	}

	public void tryToPutShip(Ship ship) {
		if (isShipPlaceable(ship)) {
			ships.add(ship);
			ship.putDown();
		}
	}

	private boolean isShipPlaceable(Ship ship) {
		return !isShipInTheWay(ship) && areFieldsOfShipValid(ship);
	}

	private boolean isShipInTheWay(Ship ship) {
		boolean inTheWay = false;
		for (Ship placedShip : ships) {
			if (placedShip.isInTheWayOfShip(ship)) {
				inTheWay = true;
			}
		}
		return inTheWay;
	}

	private boolean areFieldsOfShipValid(Ship ship) {
		return ship.areFieldsValidOnSize(sizeX, sizeY);
	}

	public CoordinatePair generateRandomCoordinatesForShipType(ShipType shipType) {
		int minX = shipType.getMinX();
		int maxX = shipType.getMaxX();
		int minY = shipType.getMinY();
		int maxY = shipType.getMaxY();

		int x = new Random().nextInt(sizeX + 3 - maxX) - minX;
		int y = new Random().nextInt(sizeY + 3 - maxY) - minY;
		return new CoordinatePair(x, y);
	}

	public List<CoordinatePair> generatePossibleCoordinatesForShipType(ShipType shipType) {
		List<CoordinatePair> coordinates = new ArrayList<>();
		int minX = shipType.getMinX();
		int maxX = shipType.getMaxX();
		int minY = shipType.getMinY();
		int maxY = shipType.getMaxY();
		for (int x = -minX; x < sizeX + 2 - maxX; x++) {
			for (int y = -minY; y < sizeY + 2 - maxY; y++) {
				coordinates.add(new CoordinatePair(x, y));
			}
		}
		return coordinates;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < sizeX; i++) {
			for (int j = 0; j < sizeY; j++) {
				CoordinatePair coordinatePair = new CoordinatePair(i, j);
				boolean printed = false;
				for (Ship ship : ships) {
					if (ship.isAtCoordinates(coordinatePair) && ship.hasStatusAt(FieldStatus.SHIP, coordinatePair)) {
						sb.append("O ");
						printed = true;
					} else if (ship.isAtCoordinates(coordinatePair) && ship.hasStatusAt(FieldStatus.DESTROYED, coordinatePair)) {
						sb.append("X ");
						printed = true;
					}
				}
				if (!printed) {
					sb.append("~ ");
				}
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	public ShotResult getShotAt(CoordinatePair coordinates) {
		if (!coordinates.validOnSize(sizeX, sizeY)) {
			throw new IllegalArgumentException("Invalid target coordinates: " + coordinates);
		}
		ShotResult result = ShotResult.MISS;
		for (Ship ship : ships) {
			if (ship.isAtCoordinates(coordinates)) {
				result = ship.getShotAt(coordinates);
			}
		}
		boolean lost = false;
		if (result.equals(ShotResult.SUNK)) {
			lost = true;
			for (Ship ship : ships) {
				if (!ship.isDead()) {
					lost = false;
				}
			}
		}

		return lost ? ShotResult.LOST : result;
	}
}
