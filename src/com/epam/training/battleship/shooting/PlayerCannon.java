package com.epam.training.battleship.shooting;

import com.epam.training.battleship.elements.basic.CoordinatePair;
import com.epam.training.battleship.table.PlayerTable;

public class PlayerCannon {

	private PlayerTable table;

	public PlayerCannon(PlayerTable table) {
		super();
		this.table = table;
	}

	public ShotResult shootAt(int x, int y) {
		CoordinatePair target = new CoordinatePair(x, y);
		return table.getShotAt(target);
	}
}
