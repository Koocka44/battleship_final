package com.epam.training.battleship.shooting;

public enum ShotResult {
	HIT, SUNK, MISS, LOST
}