package com.epam.training.battleship.shooting.algorithms;

public enum TargetingMode {
	SEARCH, DESTROY
}
