package com.epam.training.battleship.shooting.algorithms;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.epam.training.battleship.elements.basic.CoordinatePair;
import com.epam.training.battleship.elements.basic.Field;
import com.epam.training.battleship.elements.ship.Ship;
import com.epam.training.battleship.elements.ship.ShipType;
import com.epam.training.battleship.shooting.ShotResult;
import com.epam.training.battleship.table.EnemyTable;

public class SuperSecretShootingAlgorithm {

	private EnemyTable enemyTable;
	private List<ShipType> shipTypes;
	private List<Ship> possibleShips;
	private Ship currentShip;
	private TargetingMode targetingMode = TargetingMode.SEARCH;
	private Set<CoordinatePair> dontShootAt;
	private List<CoordinatePair> listForRandom;
	private CoordinatePair target = null;
	private boolean possibleTypesSelected = false;
	private ShotResult response = null;

	public SuperSecretShootingAlgorithm(EnemyTable enemyTable, Map<ShipType, Integer> shipTypes) {
		this.enemyTable = enemyTable;
		this.dontShootAt = new HashSet<CoordinatePair>();
		this.shipTypes = new ArrayList<ShipType>();
		this.possibleShips = new ArrayList<>();
		for (ShipType type : shipTypes.keySet()) {
			for (int i = 0; i < shipTypes.get(type); i++) {
				this.shipTypes.add(type);
			}
		}
		this.listForRandom = new ArrayList<>();
		fillRandom();
	}

	private void fillRandom() {
		for (int x = 0; x < enemyTable.getSizeX(); x++) {
			for (int y = 0; y < enemyTable.getSizeY(); y++) {
				listForRandom.add(new CoordinatePair(x, y));
			}
		}
	}

	public CoordinatePair getNextTarget() {
		if (targetingMode.equals(TargetingMode.SEARCH)) {
			System.out.println("search mode");
			possibleShips.clear();
			possibleTypesSelected = false;
			do {
				Random generator = new Random();
				int index = generator.nextInt(listForRandom.size());
				target = listForRandom.get(index);
				listForRandom.remove(index);
				if(listForRandom.size() == 0){
					fillRandom();
				}
			} while (dontShootAt.contains(target));
		} else if (targetingMode.equals(TargetingMode.DESTROY)) {
			System.out.println("destroy mode");
			if (!possibleTypesSelected) {
				selectPossibleShipTypes();
			}
			if (possibleShips.size() == 0) {
				possibleTypesSelected = false;
				targetingMode = TargetingMode.SEARCH;
				target = getNextTarget();
			} else {
				possibleTypesSelected = true;
				currentShip = possibleShips.get(0);

				if (response.equals(ShotResult.MISS)) {
					possibleShips.remove(currentShip);
					Iterator<Ship> iterator = possibleShips.iterator();
					while (iterator.hasNext()) {
						if (iterator.next().isAtCoordinates(target)) {
							iterator.remove();
						}
					}
					if (possibleShips.size() == 0) {
						possibleTypesSelected = false;
						targetingMode = TargetingMode.SEARCH;
						target = getNextTarget();
					} else {
						currentShip = possibleShips.get(0);
						target = currentShip.getNextFieldCoordinates();
					}
				} else if (response.equals(ShotResult.HIT)) {
					if (!currentShip.hasMoreFields()) {
						possibleShips.remove(0);
						if (possibleShips.size() == 0) {
							possibleTypesSelected = false;
							targetingMode = TargetingMode.SEARCH;
							target = getNextTarget();
						} else {
							currentShip = possibleShips.get(0);
							target = currentShip.getNextFieldCoordinates();
						}
					} else {
						target = currentShip.getNextFieldCoordinates();
					}
				} else if (response.equals(ShotResult.SUNK)) {
					shipTypes.remove(currentShip.getShipType());
					dontShootAt.clear();
					dontShootAt.addAll(enemyTable.generateDontShootCoordinates());
					possibleTypesSelected = false;
					targetingMode = TargetingMode.SEARCH;
					target = getNextTarget();
				}
			}
		}
		dontShootAt.add(target);
		return target;
	}

	private void selectPossibleShipTypes() {
		for (ShipType shipType : shipTypes) {
			for (Field field : shipType.getFields()) {
				CoordinatePair baseCoordinates = new CoordinatePair(target.getX() - field.getX(), target.getY() - field.getY());
				Ship testShip = new Ship(shipType, baseCoordinates);
				boolean possible = true;
				for (Field shipField : testShip.getFields()) {
					if (!shipField.isAtCoordinates(target) && dontShootAt.contains(shipField.getCoordinates())) {
						possible = false;
					}
				}
				if (possible && enemyTable.wouldShipFit(testShip, target)) {
					possibleShips.add(testShip);
				}
			}
		}
	}

	public void setTargetingMode(TargetingMode mode) {
		this.targetingMode = mode;
	}

	public void setResponse(ShotResult response) {
		this.response = response;
	}

}
