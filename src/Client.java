import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

import com.epam.training.battleship.elements.basic.CoordinatePair;
import com.epam.training.battleship.elements.basic.FieldStatus;
import com.epam.training.battleship.shooting.ShotResult;
import com.epam.training.battleship.table.EnemyTable;
import com.epam.training.battleship.table.EnemyTableHandler;
import com.epam.training.battleship.table.PlayerTable;
import com.epam.training.battleship.table.PlayerTableHandler;

public class Client {

	private static final String SHIPS_FILE = "ships.txt";
	private static Socket clientSocket = null;
	private static BufferedReader input = null;
	private static PrintStream output = null;
	private static EnemyTable enemyTable = null;
	private static PlayerTable myTable = null;
	private static EnemyTableHandler enemyTableHandler = null;
	private static PlayerTableHandler myTableHandler = null;
	private static CoordinatePair target;
	private static boolean gameOver = false;

	public static void main(String[] args) {

		// int port = Integer.parseInt(args[0]);
		// int width = Integer.parseInt(args[1]);
		// int height = Integer.parseInt(args[2]);

		int port = 3251;

		try {
			clientSocket = new Socket("localhost", port);
			clientSocket.setTcpNoDelay(true);
			System.out.println("Connected.");
			input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			output = new PrintStream(clientSocket.getOutputStream());
			System.out.println("Waiting for enemy HELLO message.");
			String enemyMessage = waitForEnemy();
			processMessage(enemyMessage);
			while (!gameOver) {
				target = new CoordinatePair(2, 2);
				sendFireMessage(target);
				enemyMessage = waitForEnemy();
				processMessage(enemyMessage);
				if (!gameOver) {
					enemyMessage = waitForEnemy();
					processMessage(enemyMessage);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static final String waitForEnemy() throws IOException {
		System.out.println("Waiting for enemy.");
		String line = input.readLine();
		System.out.println("Received message:" + line);
		return line;
	}

	private static final void processMessage(String message) throws IOException {
		if (message.contains("HIT")) {
			addEnemyField(FieldStatus.DESTROYED);
			System.out.println("it was a hit.");
		} else if (message.contains("SUNK")) {
			addEnemyField(FieldStatus.DESTROYED);
			System.out.println("it was a hit-sunk.");
		} else if (message.contains("LOST")) {
			gameOver = true;
			addEnemyField(FieldStatus.DESTROYED);
			System.out.println("We won!");
		} else if (message.contains("MISS")) {
			addEnemyField(FieldStatus.WATER);
		} else if (message.contains("FIRE")) {
			String[] parts = message.split(" ");
			int x = Integer.parseInt(parts[1]);
			int y = Integer.parseInt(parts[2]);
			ShotResult result = myTable.getShotAt(new CoordinatePair(x, y));
			sendResultMessage(result);
		} else if (message.contains("HELLO")) {
			String[] parts = message.split(" ");
			int width = Integer.parseInt(parts[1]);
			int height = Integer.parseInt(parts[2]);
			myTable = new PlayerTable(width, height);
			enemyTable = new EnemyTable(width, height);
			myTableHandler = new PlayerTableHandler(myTable);
			enemyTableHandler = new EnemyTableHandler(enemyTable);
			myTableHandler.generateTableFromFile(SHIPS_FILE);
			System.out.println(myTable);
		} else {
			throw new IllegalArgumentException("invalid message: " + message);
		}
	}

	private static void addEnemyField(FieldStatus destroyed) {
		enemyTableHandler.addField(target, destroyed);

	}

	private static final void sendResultMessage(ShotResult result) throws IOException {
		String message;
		if (result.equals(ShotResult.HIT)) {
			message = "HIT";
		} else if (result.equals(ShotResult.MISS)) {
			message = "MISS";
		} else if (result.equals(ShotResult.SUNK)) {
			message = "SUNK";
		} else if (result.equals(ShotResult.LOST)) {
			gameOver = true;
			message = "LOST";
		} else {
			throw new IllegalStateException("invalid shot result from player table");
		}
		sendMessage(message);
	}

	private static final void sendMessage(String message) throws IOException {
		System.out.println("sending message: " + message);
		output.println(message);
	}

	private static final void sendFireMessage(CoordinatePair coordinates) throws IOException {
		String message = String.format("FIRE %d %d", coordinates.getX(), coordinates.getY());
		sendMessage(message);
	}
}