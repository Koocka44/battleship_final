package com.epam.training.battleship.elements.basic;

import static org.junit.Assert.assertEquals;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.epam.training.battleship.elements.basic.CoordinatePair;
import com.epam.training.battleship.elements.basic.Field;
import com.epam.training.battleship.elements.basic.FieldStatus;

public class TestField {

	private Field underTest;
	private CoordinatePair coordinateMock;

	@Before
	public void setUp() {
		coordinateMock = EasyMock.createMock(CoordinatePair.class);
		underTest = new Field(coordinateMock, FieldStatus.WATER);
	}

	@Test
	public void testGenereateWorldCoordinatesToReference() {
		// GIVEN
		CoordinatePair reference = new CoordinatePair(3, 3);
		CoordinatePair expectedCoordinates = new CoordinatePair(5, 5);
		Field expected = new Field(expectedCoordinates, FieldStatus.WATER);
		EasyMock.expect(coordinateMock.generateWorldCoordinatesFromReference(reference)).andReturn(expectedCoordinates);
		EasyMock.replay(coordinateMock);
		// WHEN
		Field result = underTest.genereateWorldCoordinatesToReference(reference);
		// THEN
		assertEquals(expected, result);
	}
}
