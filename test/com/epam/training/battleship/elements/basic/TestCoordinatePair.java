package com.epam.training.battleship.elements.basic;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.epam.training.battleship.elements.basic.CoordinatePair;

public class TestCoordinatePair {

	private CoordinatePair underTest;

	@Before
	public void setUp() {
		this.underTest = new CoordinatePair(5, 5);
	}

	@Test
	public void testGenerateWorldCoordinatesFromReferenceShouldReturnValidResult() {
		// GIVEN
		CoordinatePair referenceCoordinates = new CoordinatePair(1, 1);
		CoordinatePair expected = new CoordinatePair(6, 6);
		// WHEN
		CoordinatePair result = underTest.generateWorldCoordinatesFromReference(referenceCoordinates);
		// THEN
		assertEquals(expected, result);
	}
}
