package com.epam.training.battleship.elements.ship;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import com.epam.training.battleship.elements.basic.CoordinatePair;
import com.epam.training.battleship.elements.basic.Field;
import com.epam.training.battleship.elements.basic.FieldStatus;
import com.epam.training.battleship.elements.ship.ShipType;

public class TestShipType {

	private ShipType underTest;
	private Field fieldMock;

	@Before
	public void setUp() {
		fieldMock = EasyMock.createMock(Field.class);
		Set<Field> fields = new HashSet<>();
		fields.add(fieldMock);
		underTest = new ShipType(fields);
	}

	@Test
	public void testGetFieldStatusAtShouldReturnCorrectStatusWhenFieldExists() {
		// GIVEN
		FieldStatus expected = FieldStatus.SHIP;
		CoordinatePair coordinates = new CoordinatePair(5, 5);
		EasyMock.expect(fieldMock.isAtCoordinates(coordinates)).andReturn(true);
		EasyMock.expect(fieldMock.getStatus()).andReturn(expected);
		EasyMock.replay(fieldMock);
		// WHEN
		FieldStatus result = underTest.getFieldStatusAt(coordinates);
		// THEN
		assertEquals(expected, result);
	}

	@Test
	public void testGetFieldStatusAtShouldReturnCorrectStatusWhenFieldDoesntExist() {
		// GIVEN
		FieldStatus expected = FieldStatus.WATER;
		CoordinatePair coordinates = new CoordinatePair(5, 5);
		EasyMock.expect(fieldMock.isAtCoordinates(coordinates)).andReturn(false);
		EasyMock.replay(fieldMock);
		// WHEN
		FieldStatus result = underTest.getFieldStatusAt(coordinates);
		// THEN
		assertEquals(expected, result);
	}

	@Test
	public void testGenerateFieldsToReferenceCoordinates() {
		// GIVEN
		CoordinatePair referenceCoordinates = new CoordinatePair(2, 2);
		CoordinatePair resultCoordinatePair = new CoordinatePair(5, 5);
		Field expectedField = new Field(resultCoordinatePair, FieldStatus.SHIP);
		Set<Field> expectedSet = new HashSet<>();
		expectedSet.add(expectedField);
		EasyMock.expect(fieldMock.genereateWorldCoordinatesToReference(referenceCoordinates)).andReturn(expectedField);
		EasyMock.replay(fieldMock);
		// WHEN
		Set<Field> resultSet = underTest.generateFieldsToReferenceCoordinates(referenceCoordinates);
		// THEN
		assertEquals(expectedSet, resultSet);
	}
}
